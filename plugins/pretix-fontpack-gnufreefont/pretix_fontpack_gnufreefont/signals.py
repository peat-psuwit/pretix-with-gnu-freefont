from django.dispatch import receiver
from django.utils.safestring import mark_safe

from pretix.plugins.ticketoutputpdf.signals import register_fonts

@receiver(register_fonts, dispatch_uid="fontpack_gnufreefont")
def fontpack_gnufreefont(sender, **kwargs):
    basepath = 'pretix_fontpack_gnufreefont'

    # Generated using generate_font_table.js.
    # Please make sure generate_font_table.js is in sync.
    return {
        "FreeMono": {
            "regular": {
                "truetype": basepath + "/freefont-ttf/FreeMono.ttf",
                "woff": basepath + "/freefont-woff/FreeMono.woff",
            },
            "bold": {
                "truetype": basepath + "/freefont-ttf/FreeMonoBold.ttf",
                "woff": basepath + "/freefont-woff/FreeMonoBold.woff",
            },
            "italic": {
                "truetype": basepath + "/freefont-ttf/FreeMonoOblique.ttf",
                "woff": basepath + "/freefont-woff/FreeMonoOblique.woff",
            },
            "bolditalic": {
                "truetype": basepath + "/freefont-ttf/FreeMonoBoldOblique.ttf",
                "woff": basepath + "/freefont-woff/FreeMonoBoldOblique.woff",
            },
            "sample": mark_safe(
                "ผู้ใหญ่ลีรู้ทฤษฎีน้ำแข็ง"
            )
        },
        "FreeSans": {
            "regular": {
                "truetype": basepath + "/freefont-ttf/FreeSans.ttf",
                "woff": basepath + "/freefont-woff/FreeSans.woff",
            },
            "bold": {
                "truetype": basepath + "/freefont-ttf/FreeSansBold.ttf",
                "woff": basepath + "/freefont-woff/FreeSansBold.woff",
            },
            "italic": {
                "truetype": basepath + "/freefont-ttf/FreeSansOblique.ttf",
                "woff": basepath + "/freefont-woff/FreeSansOblique.woff",
            },
            "bolditalic": {
                "truetype": basepath + "/freefont-ttf/FreeSansBoldOblique.ttf",
                "woff": basepath + "/freefont-woff/FreeSansBoldOblique.woff",
            },
            "sample": mark_safe(
                "ผู้ใหญ่ลีรู้ทฤษฎีน้ำแข็ง"
            )
        },
        "FreeSerif": {
            "regular": {
                "truetype": basepath + "/freefont-ttf/FreeSerif.ttf",
                "woff": basepath + "/freefont-woff/FreeSerif.woff",
            },
            "bold": {
                "truetype": basepath + "/freefont-ttf/FreeSerifBold.ttf",
                "woff": basepath + "/freefont-woff/FreeSerifBold.woff",
            },
            "italic": {
                "truetype": basepath + "/freefont-ttf/FreeSerifItalic.ttf",
                "woff": basepath + "/freefont-woff/FreeSerifItalic.woff",
            },
            "bolditalic": {
                "truetype": basepath + "/freefont-ttf/FreeSerifBoldItalic.ttf",
                "woff": basepath + "/freefont-woff/FreeSerifBoldItalic.woff",
            },
            "sample": mark_safe(
                "ผู้ใหญ่ลีรู้ทฤษฎีน้ำแข็ง"
            )
        },
    }
