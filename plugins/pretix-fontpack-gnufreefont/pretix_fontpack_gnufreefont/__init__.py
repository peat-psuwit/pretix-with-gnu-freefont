from django.utils.translation import gettext_lazy

try:
    from pretix.base.plugins import PluginConfig
except ImportError:
    raise RuntimeError("Please use pretix 2.7 or above to run this plugin!")

__version__ = '1.0.0'


class PluginApp(PluginConfig):
    name = 'pretix_fontpack_gnufreefont'
    verbose_name = 'Pretix GNU FreeFont Fontpack'

    class PretixPluginMeta:
        name = gettext_lazy('Pretix GNU FreeFont Fontpack')
        author = 'Ratchanan Srirattanamet'
        description = gettext_lazy(' GNU FreeFont packaged to be used for pretix\' ticket editor')
        visible = True
        version = __version__
        category = 'CUSTOMIZATION'
        compatibility = "pretix>=2.7.0"

    def ready(self):
        from . import signals  # NOQA


default_app_config = 'pretix_fontpack_gnufreefont.PluginApp'
