// Script to generate table in signals.py
// Run this script using Node.js then copy the result yourself.
// Why JS? Well, simply because I'm familiar with JS's templating system.

const fontSubFamilies = {
    'regular': '',
    'bold': 'Bold',
    'italic': 'Italic',
    'bolditalic': 'BoldItalic',
};

const fontSubFamiliesOblique = {
    'regular': '',
    'bold': 'Bold',
    'italic': 'Oblique',
    'bolditalic': 'BoldOblique',
};

const fontFamilies = {
    'FreeMono': fontSubFamiliesOblique,
    'FreeSans': fontSubFamiliesOblique,
    'FreeSerif': fontSubFamilies,
};

console.log('return {');
for (const [fontFamily, subFams] of Object.entries(fontFamilies)) {
    console.log(`    "${fontFamily}": {`);
    for (const [subFamily, subFamilyFile] of Object.entries(subFams)) {
        console.log(`        "${subFamily}": {`);
        console.log(`            "truetype": basepath + "/freefont-ttf/${fontFamily}${subFamilyFile}.ttf",`);
        console.log(`            "woff": basepath + "/freefont-woff/${fontFamily}${subFamilyFile}.woff",`);
        console.log('        },');
    }
    console.log('        "sample": mark_safe(');
    console.log('            "ผู้ใหญ่ลีรู้ทฤษฎีน้ำแข็ง"');
    console.log('        )');
    console.log('    },');
}
console.log('}');
