import os
from distutils.command.build import build

from setuptools import find_packages, setup

from pretix_fontpack_gnufreefont import __version__


try:
    with open(os.path.join(os.path.dirname(__file__), 'README.rst'), encoding='utf-8') as f:
        long_description = f.read()
except:
    long_description = ''

setup(
    name='pretix-fontpack-gnufreefont',
    version=__version__,
    description='GNU FreeFont packaged to be used with pretix\' ticket editor',
    long_description=long_description,
    url='https://gitlab.com/peat-psuwit/pretix-with-gnu-freefont/-/tree/master/plugins/pretix-fontpack-gnufreefont',
    author='Ratchanan Srirattanamet',
    author_email='peat@peat-network.xyz',
    license='Apache',

    install_requires=[],
    packages=find_packages(exclude=['tests', 'tests.*']),
    include_package_data=True,
    entry_points="""
[pretix.plugin]
pretix_fontpack_gnufreefont=pretix_fontpack_gnufreefont:PretixPluginMeta
""",
)
