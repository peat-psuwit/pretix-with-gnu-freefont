# See https://docs.pretix.eu/en/latest/admin/installation/docker_smallscale.html#install-a-plugin

# Allow using GitLab as a proxy to avoid Docker Hub limits.
# https://docs.gitlab.com/ee/user/packages/dependency_proxy/
# https://www.jeffgeerling.com/blog/2017/use-arg-dockerfile-dynamic-image-specification
ARG IMAGE_ORIGIN=docker.io
# Check new version: https://pretix.eu/about/en/blog/
FROM $IMAGE_ORIGIN/pretix/standalone:2025.1

USER root

# Upgrade all packages in the base image.
# https://pythonspeed.com/articles/security-updates-in-docker/
RUN \
    set -eux; \
    apt-get update; \
    apt-get upgrade -y; \
    rm -rf /var/lib/apt/lists/*

COPY plugins/pretix-fontpack-gnufreefont /usr/src/pretix-fontpack-gnufreefont
RUN pip3 install /usr/src/pretix-fontpack-gnufreefont --no-index

USER pretixuser
RUN cd /pretix/src && make production
