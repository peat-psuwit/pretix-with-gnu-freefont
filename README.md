Pretix (with GNU FreeFont)
--------------------------

Pretix's default Docker image comes with a single font, suitable for European text only. This Docker image adds fonts from [GNU FreeFont](https://www.gnu.org/software/freefont/) which, at least for FreeSerif, contains wider range of glyphs, including Thai language.